//
//  Accessory.swift
//  kindergarten-ios
//
//  Created by Adrian Sandu on 6/7/17.
//  Copyright © 2017 Adrian Sandu. All rights reserved.
//

import UIKit

enum FaceElement: Int {
    case Hat = 0
    case Eyes = 1
    case Nose = 2
    case Mouth = 3
    case Count = 4
}

class Accessory {
    
    var image: UIImage
    var scale: CGFloat
    var element: FaceElement
    
    init(image: UIImage, scale: CGFloat, faceElement: FaceElement) {
        self.image = image
        self.scale = scale
        self.element = faceElement
    }
}
