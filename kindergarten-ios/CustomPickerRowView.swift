//
//  CustomPickerRowView.swift
//  kindergarten-ios

//  Created by Ana Maria Dumitrache on 5/26/17.
//  Copyright © 2017 Adrian Sandu. All rights reserved.
//


import UIKit
import PureLayout

class CustomPickerRowView: UIView {
    
    // MARK: - IBOutlets
    
    // MARK: - Properties
    let rowData:RowData
    
    var imageView:UIImageView!
    var label:UILabel!
    
    var didSetupConstraints:Bool = false
    
    // MARK: - Initializers methods
    init(frame: CGRect, rowData:RowData) {
        
        self.rowData = rowData
        
        super.init(frame: frame)
        
      
        
        createImageView()
        createLabel()
        
     
        label.autoCenterInSuperview()
        imageView.autoPinEdge(.trailing, to: .leading,of: label , withOffset: 30)
      
    }
    
    required init(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
        
    }
    
    // MARK: - Private methods
    fileprivate func createImageView(){
        
        imageView = UIImageView.newAutoLayout()
        imageView.image = UIImage(named: rowData.imageName)
        addSubview(imageView)
        
    }
    
    fileprivate func createLabel(){
        
        label = UILabel.newAutoLayout()
      //  label.backgroundColor = UIColor.red
        label.textColor=UIColor.white
        
        label.text = rowData.title
        addSubview(label)
        
    }
    
    // MARK: - Public methods
    
    // MARK: - Getter & setter methods
    
    // MARK: - IBActions
    
    // MARK: - Target-Action methods
    
    // MARK: - Notification handler methods
    
    // MARK: - Datasource methods
    
    // MARK: - Delegate methods
    
}

