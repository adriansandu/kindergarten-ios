//
//  FaceDetector.swift
//  kindergarten-ios
//
//  Created by Adrian Sandu on 4/12/17.
//  Copyright © 2017 Adrian Sandu. All rights reserved.
//

import Foundation
import CoreImage
import AVFoundation

class FaceDetector {
    var detector: CIDetector?
    var options: [String : AnyObject]?
    var context: CIContext?
    
    init() {
        context = CIContext()
        
        options = [String : AnyObject]()
        options![CIDetectorAccuracy] = CIDetectorAccuracyLow as AnyObject?
        
        detector = CIDetector(ofType: CIDetectorTypeFace, context: context!, options: options!)
    }
    
    func getFacialFeaturesFromImage(_ image: CIImage, options: [String : AnyObject]) -> [CIFeature] {
        return self.detector!.features(in: image, options: options)
    }
}
