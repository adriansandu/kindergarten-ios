//
//  FaceMask.swift
//  kindergarten-ios
//
//  Created by Adrian Sandu on 6/7/17.
//  Copyright © 2017 Adrian Sandu. All rights reserved.
//

import UIKit

class FaceMask {
    
    var accessories: [Accessory] = [] {
        didSet {
            for accessory in accessories {
                let imageView = accessoryImageView[accessory.element.rawValue]
                imageView.image = accessory.image
                imageView.frame = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
                imageView.contentMode = .scaleAspectFit
            }
        }
    }

    let accessoryImageView = [UIImageView](repeating: UIImageView(), count: FaceElement.Count.rawValue)
}
