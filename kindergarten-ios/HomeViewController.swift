//
//  HomeViewController.swift
//  kindergarten-ios
//
//  Created by Ana Maria Dumitrache on 5/23/17.
//  Copyright © 2017 Ana Maria Dumitrache. All rights reserved.
//

import UIKit

var animalsData:[RowData] = [
      RowData(imageName: "cow", title: ""),
      RowData(imageName: "dog", title: ""),
      RowData(imageName: "cat", title: ""),
      RowData(imageName: "pig", title: ""),
      RowData(imageName: "rabbit", title: "")

]

extension UIButton{
    func roundedButton(){
        let maskPAth1 = UIBezierPath(roundedRect: self.bounds,byRoundingCorners: [.topLeft , .topRight,.bottomLeft , .bottomRight ],cornerRadii:CGSize(width : 10.0,height: 10.0))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = self.bounds
        maskLayer1.path = maskPAth1.cgPath
        self.layer.mask = maskLayer1
        
    }
}

class HomeViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

 
    @IBOutlet weak var startButton: UIButton!
  

    @IBOutlet weak var animalPicker: UIPickerView!
    
    var data:[RowData]!
    var selectedRow:Int = 0
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        animalPicker.delegate = self
        startButton.roundedButton()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.unselectedItemTintColor = UIColor(red: 255, green: 255, blue: 255, alpha: 0.4)
        self.tabBarController?.tabBar.tintColor = UIColor.white
    }
    

  
    @IBAction func butonnav(_ sender: Any) {
        performSegue(withIdentifier: "ident", sender: selectedRow)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let sec = segue.destination as? VideoCaptureController{
            if let name = sender as? Int {
                sec.animal = name
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    
        return animalsData[row].title
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
       // var counter: Int!
        return animalsData.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedRow = row
    }
    
    
    // MARK: - Delegate methods
    // MARK: UIPickerViewDelegate methods
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        
        return view.bounds.size.width
        
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        
        return 75.0
        
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {

        let customView = CustomPickerRowView(frame: CGRect.zero, rowData: animalsData[row])
        
        return customView
        
    }

}
