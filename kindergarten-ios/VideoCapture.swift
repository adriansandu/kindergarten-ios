//
//  VideoCapture.swift
//  kindergarten-ios
//
//  Created by Adrian Sandu on 4/12/17.
//  Copyright © 2017 Adrian Sandu. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit
import CoreMotion
import ImageIO

class VideoCapture: NSObject, AVCaptureVideoDataOutputSampleBufferDelegate {
    var isCapturing: Bool = false
    var session: AVCaptureSession?
    var device: AVCaptureDevice?
    var input: AVCaptureInput?
    var preview: CALayer?
    var faceDetector: FaceDetector?
    var dataOutput: AVCaptureVideoDataOutput?
    var dataOutputQueue: DispatchQueue?
    var previewView: UIView?
    
    var animalNumber: Int!
    
    lazy var accessoryImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: animalsData[self.animalNumber].imageName + "Ears"))
        imageView.contentMode = .scaleAspectFit
        imageView.frame = CGRect(origin: CGPoint(), size: CGSize(width: 1, height: 1))
        return imageView
    }()
    
    lazy var accessoryMouthImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: animalsData[self.animalNumber].imageName + "Nose"))
        imageView.contentMode = .scaleAspectFit
        imageView.frame = CGRect(origin: CGPoint(), size: CGSize(width: 1, height: 1))
        return imageView
    }()

    
    enum VideoCaptureError: Error {
        case sessionPresetNotAvailable
        case inputDeviceNotAvailable
        case inputCouldNotBeAddedToSession
        case dataOutputCouldNotBeAddedToSession
    }
    
    override init() {
        super.init()
        
        device = VideoCaptureDevice.create()

        faceDetector = FaceDetector()
    }
    
    /* Set the camera resolution */
    
    fileprivate func setSessionPreset() throws {
        if (session!.canSetSessionPreset(AVCaptureSessionPresetHigh)) {
            session!.sessionPreset = AVCaptureSessionPresetHigh
        } else {
            throw VideoCaptureError.sessionPresetNotAvailable
        }
    }
    
    fileprivate func setDeviceInput() throws {
        do {
            self.input = try AVCaptureDeviceInput(device: self.device)
        } catch {
            throw VideoCaptureError.inputDeviceNotAvailable
        }
    }
    
    fileprivate func addInputToSession() throws {
        if (session!.canAddInput(self.input)) {
            session!.addInput(self.input)
        } else {
            throw VideoCaptureError.inputCouldNotBeAddedToSession
        }
    }
    
    fileprivate func addPreviewToView(_ view: UIView) {
        self.preview = AVCaptureVideoPreviewLayer(session: session!)
        self.preview!.frame = view.bounds
        
        view.layer.addSublayer(self.preview!)
    }
    
    func updatePreviewLayerFrame(_ view: UIView) {
        self.preview!.frame = view.bounds
    }
    
    fileprivate func stopSession() {
        if let runningSession = session {
            runningSession.stopRunning()
        }
    }
    
    fileprivate func removePreviewFromView() {
        if let previewLayer = preview {
            previewLayer.removeFromSuperlayer()
        }
    }
    
    fileprivate func setDataOutput() {
        self.dataOutput = AVCaptureVideoDataOutput()
        
        var videoSettings = [AnyHashable: Any]()
        videoSettings[kCVPixelBufferPixelFormatTypeKey as AnyHashable] = Int(CInt(kCVPixelFormatType_32BGRA))
        
        self.dataOutput!.videoSettings = videoSettings
        self.dataOutput!.alwaysDiscardsLateVideoFrames = true
        
        self.dataOutputQueue = DispatchQueue(label: "VideoDataOutputQueue", attributes: [])
        
        self.dataOutput!.setSampleBufferDelegate(self, queue: self.dataOutputQueue!)
    }
    
    fileprivate func addDataOutputToSession() throws {
        if (self.session!.canAddOutput(self.dataOutput!)) {
            self.session!.addOutput(self.dataOutput!)
        } else {
            throw VideoCaptureError.dataOutputCouldNotBeAddedToSession
        }
    }
    
    fileprivate func getImageFromBuffer(_ buffer: CMSampleBuffer) -> CIImage {
        let pixelBuffer = CMSampleBufferGetImageBuffer(buffer)
        
        let attachments = CMCopyDictionaryOfAttachments(kCFAllocatorDefault, buffer, kCMAttachmentMode_ShouldPropagate)
        
        let image = CIImage(cvPixelBuffer: pixelBuffer!, options: attachments as? [String : AnyObject])

        return image
    }
    
    fileprivate func getFacialFeaturesFromImage(_ image: CIImage) -> [CIFeature] {
        let imageOptions = [CIDetectorImageOrientation : 6]
        return self.faceDetector!.getFacialFeaturesFromImage(image, options: imageOptions as [String : AnyObject])
    }
    
    fileprivate func transformFacialFeaturePosition(_ xPosition: CGFloat, yPosition: CGFloat, videoRect: CGRect, previewRect: CGRect, isMirrored: Bool) -> CGRect {
        
        var featureRect = CGRect(origin: CGPoint(x: xPosition, y: yPosition), size: CGSize(width: 0, height: 0))
        let widthScale = previewRect.size.width / videoRect.size.height
        let heightScale = previewRect.size.height / videoRect.size.width
        
        let transform = isMirrored ? CGAffineTransform(a: 0, b: heightScale, c: -widthScale, d: 0, tx: previewRect.size.width, ty: 0) :
            CGAffineTransform(a: 0, b: heightScale, c: widthScale, d: 0, tx: 0, ty: 0)
        
        featureRect = featureRect.applying(transform)
        
        featureRect = featureRect.offsetBy(dx: previewRect.origin.x, dy: previewRect.origin.y)
        
        return featureRect
    }
    
    fileprivate func positionEyeViewInPreview(mouthPosition: CGPoint,leftEyePosition: CGPoint, rightEyePosition: CGPoint, cleanAperture: CGRect) {
        
        let isMirrored = preview!.contentsAreFlipped()
        let previewBox = preview!.frame
        
        // Get eyes position inside the preview view layer
        let rightEyeFrame = transformFacialFeaturePosition(leftEyePosition.x, yPosition: leftEyePosition.y, videoRect: cleanAperture, previewRect: previewBox, isMirrored: isMirrored)
        let leftEyeFrame = transformFacialFeaturePosition(rightEyePosition.x, yPosition: rightEyePosition.y, videoRect: cleanAperture, previewRect: previewBox, isMirrored: isMirrored)
        
        let mouthFrame = transformFacialFeaturePosition(mouthPosition.x, yPosition: mouthPosition.y, videoRect: cleanAperture, previewRect: previewBox, isMirrored: isMirrored)

        // Get the distance between the eyes
        let distanceBetweenTheEyes = distance(leftEyeFrame.origin, rightEyeFrame.origin)
        let accessoryScale: CGFloat = distanceBetweenTheEyes * 2.0
      //  let accessoryScaleMouth: CGFloat = distance(mouthFrame., mouthFrame.maxX) * 2.0

        // Get the middle point between the eyes
        let eyesMiddlePoint = CGPoint(
            x: leftEyeFrame.origin.x +
                (rightEyeFrame.origin.x - leftEyeFrame.origin.x) / 2.0,
            y: leftEyeFrame.origin.y +
                (rightEyeFrame.origin.y - leftEyeFrame.origin.y) / 2.0)
        
        
//        let eyesMiddlePoint = CGPoint(
//            x: leftEyeFrame.origin.x,
//            y: leftEyeFrame.origin.y)

        // Get the angle between the points
        let deltaY = rightEyeFrame.origin.y - leftEyeFrame.origin.y
        let deltaX = rightEyeFrame.origin.x - leftEyeFrame.origin.x
        
        let angle = atan(deltaY / deltaX)

        accessoryImageView.transform = CGAffineTransform.identity
            .translatedBy(x: eyesMiddlePoint.x, y: eyesMiddlePoint.y - 140)
            .scaledBy(x: accessoryScale, y: accessoryScale)
            .rotated(by: angle)
        
        let mouthMiddlePoint = CGPoint(
            x: mouthFrame.origin.x,
            y: mouthFrame.origin.y)
        
       
        accessoryMouthImageView.transform = CGAffineTransform.identity
            .translatedBy(x: mouthMiddlePoint.x, y: mouthMiddlePoint.y)
            .scaledBy(x: accessoryScale, y: accessoryScale)
    }
    
    
    
    fileprivate func alterPreview(_ features: [CIFeature], cleanAperture: CGRect, image: CIImage) {
        
        if (features.count == 0 || cleanAperture == CGRect.zero || !isCapturing) {
            
            // Hide everything from the screen
            accessoryImageView.isHidden = true
            accessoryMouthImageView.isHidden = true
            
            return
        }
        
        for feature in features {
            guard let faceFeature = feature as? CIFaceFeature else {
                continue
            }

            if (faceFeature.hasLeftEyePosition && faceFeature.hasRightEyePosition && faceFeature.hasMouthPosition) {
                accessoryImageView.isHidden = false
                accessoryMouthImageView.isHidden = false
                positionEyeViewInPreview(mouthPosition: faceFeature.mouthPosition, leftEyePosition: faceFeature.leftEyePosition, rightEyePosition: faceFeature.rightEyePosition, cleanAperture: cleanAperture)
            }
        }
    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, from connection: AVCaptureConnection!) {
        
        let image = getImageFromBuffer(sampleBuffer)
        
        let features = getFacialFeaturesFromImage(image)
        
        let formatDescription = CMSampleBufferGetFormatDescription(sampleBuffer)
        
        let cleanAperture = CMVideoFormatDescriptionGetCleanAperture(formatDescription!, false)
        
        DispatchQueue.main.async {
            self.alterPreview(features, cleanAperture: cleanAperture, image: image)
        }
    }
    
    func startCapturing(_ previewView: UIView, animal: Int) throws {
        isCapturing = true
        
        /* Get animal from picker */

        animalNumber = animal
        
        self.previewView = previewView
        
        self.session = AVCaptureSession()
        
        try setSessionPreset()
        
        try setDeviceInput()
        
        try addInputToSession()
        
        setDataOutput()
        
        try addDataOutputToSession()
        
        addPreviewToView(self.previewView!)
        self.previewView?.addSubview(accessoryImageView)
        self.previewView?.addSubview(accessoryMouthImageView)
        
        session!.startRunning()
    }
    
    func stopCapturing() {
        isCapturing = false
        
        stopSession()
        
        removePreviewFromView()
        accessoryImageView.removeFromSuperview()
        accessoryMouthImageView.removeFromSuperview()
        
        preview = nil
        dataOutput = nil
        dataOutputQueue = nil
        session = nil
        previewView = nil
    }
    
    func distance(_ a: CGPoint, _ b: CGPoint) -> CGFloat {
        let xDist = a.x - b.x
        let yDist = a.y - b.y
        return CGFloat(sqrt((xDist * xDist) + (yDist * yDist)))
    }
}

