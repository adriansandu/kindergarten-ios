//
//  VideoCaptureController.swift
//  kindergarten-ios
//
//  Created by Adrian Sandu on 4/12/17.
//  Copyright © 2017 Adrian Sandu. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation


class VideoCaptureController: UIViewController {
    
    var videoCapture: VideoCapture?
    var animalName: AVAudioPlayer!
  
    private var _animal = Int()
    var animal : Int {
        get {
            return _animal
        }
        set {
            _animal = newValue
        }
    }
    
    @IBOutlet weak var buttonsunet: UIButton!
    @IBOutlet weak var view1: UIView!

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        videoCapture?.updatePreviewLayerFrame(self.view1)
    }
    
    @IBAction func butonAction(_ sender: Any) {
        performSegue(withIdentifier: "iden", sender: self)
    }
    
    override func viewDidLoad() {
         videoCapture = VideoCapture()
         startCapturing(animal: _animal)
      
         print("Did select row \(_animal)")
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func takeSnap(_ sender: Any) {
        UIGraphicsBeginImageContext(view.frame.size)
        view1.layer.sublayers?.last?.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        let uiiamgev = UIImageView(image: image!)
        uiiamgev.frame = CGRect.zero
        self.view.addSubview(uiiamgev)
        
    }
    
    func takeScreenshot(view: UIView) -> UIImageView {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        UIImageWriteToSavedPhotosAlbum(image!, nil, nil, nil)
    
        return UIImageView(image: image)
    }
    
    @IBAction func playSound(_ sender: Any) {
        
        let path = Bundle.main.path(forResource: animalsData[animal].imageName, ofType:"mp3")!
        let url = URL(fileURLWithPath: path)
        
        
        do {
            let sound = try AVAudioPlayer(contentsOf: url)
            animalName = sound
            sound.play()
        } catch {
            // couldn't load file
        }
        
    }

    override func didReceiveMemoryWarning() {
        stopCapturing()
    }
    

    func startCapturing(animal: Int) {
       
        do {
            
            try videoCapture!.startCapturing(self.view1,animal: animal)
            
        }
        catch {
            // Error
        }
    }
    
    func stopCapturing() {
        videoCapture!.stopCapturing()
    }
}
