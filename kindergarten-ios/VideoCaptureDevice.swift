//
//  VideoCaptureDevice.swift
//  kindergarten-ios
//
//  Created by Adrian Sandu on 4/12/17.
//  Copyright © 2017 Adrian Sandu. All rights reserved.
//

import Foundation
import AVFoundation

class VideoCaptureDevice {
    
    static func create() -> AVCaptureDevice {
        var device: AVCaptureDevice?
        
        AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo).forEach { videoDevice in
            if ((videoDevice as AnyObject).position == AVCaptureDevicePosition.front) {
                device = videoDevice as? AVCaptureDevice
            }
        }
        
        if (nil == device) {
            device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        }
        
        return device!
    }
}
