//
//  ViewController.swift
//  kindergarten-ios
//
//  Created by Adrian Sandu on 4/12/17.
//  Copyright © 2017 Adrian Sandu. All rights reserved.
//

import UIKit

struct RowData {
    
    let imageName:String
    let title:String
    
}

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

